package com.everis.apirest.controller.resource;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class NameResource {
	private String name;
	private String descripcion;
	private Integer version;
}
