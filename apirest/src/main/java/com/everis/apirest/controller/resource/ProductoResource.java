package com.everis.apirest.controller.resource;

import java.math.BigDecimal;

import lombok.Data;


@Data
//@Getter
//@Setter
//@AllArgsConstructor
//@NoArgsConstructor

public class ProductoResource {
	private Long id;
	private String nombre;
	private String codigo;
	private String descripcion;
	private BigDecimal precio;
	private String tipoProducto;
	private Boolean activo;
}
