package com.everis.apirest.controller.resource;

import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class InfoResource {
	private String nombre;
	private String usuario;
	private String fechaSistema = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
	
}

