package com.everis.apirest.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.everis.apirest.model.entity.Producto;
import com.everis.apirest.model.entity.TipoProducto;
import com.everis.apirest.model.repository.ProductoRepository;
import com.everis.apirest.model.repository.TipoProductoRepository;

@Service
public class ProductoServiceImpl implements ProductoService{

	
	@Autowired
	private ProductoRepository productoRepository;
	
	@Autowired
	private TipoProductoRepository tipoProductoRepository;

	@Override
	public Iterable<Producto> obtenerProductos() {
		
		return productoRepository.findAll();
	}

	@Override
	public Producto insertar(Producto producto) throws Exception {
		return productoRepository.save(producto);
	}

	@Override
	public Producto obtenerProductoPorId(Long id) throws Exception {
		return productoRepository.findById(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Producto no encontrado"));
	}
	
	@Override
	public Producto modificarProducto(Producto producto) throws Exception {	
		//Optional<Producto> pro = productoRepository.findByCodigo(producto.getCodigo());

		return productoRepository.findByCodigo(producto.getCodigo())
	        .map(productoUpdate -> {
	        	productoUpdate.setNombre(producto.getNombre());
	        	productoRepository.save(productoUpdate);
	            return productoUpdate;
	    })
	        .orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Producto no encontrado"));
	}
	@Override
	public TipoProducto obtenerTipoProducto(String codigoTipoProducto) throws Exception {
		return tipoProductoRepository.findByCodigo(codigoTipoProducto).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"El códigoTipoProducto no es válido"));
	}

	
}
