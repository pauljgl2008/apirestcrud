package com.everis.apirest.model.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.apirest.model.entity.Producto;
import com.everis.apirest.model.entity.TipoProducto;

@Repository
public interface ProductoRepository extends CrudRepository<Producto, Long> {
	public Optional<Producto> findByCodigo(String codigo);

}
